import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseComponent } from '../pages/course/course.component';
// import { CourseDetailsComponent } from '../pages/seeker/course-details/course-details.component';
import { ScheduleSessionsComponent } from '../pages/schedule-sessions/schedule-sessions.component';
import { StartSessionsComponent } from '../pages/start-sessions/start-sessions.component';
import { CourseDetailsComponent } from '../pages/course-details/course-details.component';
import { AddCourseComponent } from '../pages/add-course/add-course.component';
import { ComplimentsComponent } from '../pages/compliments/compliments.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    CourseComponent,
    CourseDetailsComponent,
    AddCourseComponent,
    CourseComponent,
    // CourseDetailsComponent,
    ScheduleSessionsComponent,
    StartSessionsComponent,
    ComplimentsComponent
  ]
})
export class SharedModuleModule { }
