import { AbstractControl } from '@angular/forms';

export function ValidEmail(control: AbstractControl) {
  if (!control.value) {
    return { validUrl: true };
  }
  return null;
}