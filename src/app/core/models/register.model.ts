export interface Register {
    status: string;
    msg: string;
    link: string;
    uId: string;
    responsecode: string;
  }
