import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { ApiService } from './api.service';
import { JwtService } from './jwt.service';
import { User } from '../models';
import { map } from 'rxjs/operators/map';
import { distinctUntilChanged } from 'rxjs/operators/distinctUntilChanged';
import { urlsUser } from '../../app-constants';

@Injectable()
export class UserService {

  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();
  
  constructor(
    private apiService: ApiService,
    private http: HttpClient,
    private jwtService: JwtService
  ) { }

  setAuth(user: User) {
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(user.token);
    // Set current user data into observable
    this.currentUserSubject.next(user);
    // Set isAuthenticated to true
    this.isAuthenticatedSubject.next(true);
  }

  attemptAuth(credentials): Observable<User> {
    const route = '/authenticate/';
    return this.apiService.post('/userapi' + route, credentials)
      .pipe(map(
      data => {
        this.setAuth(data);
        return data;
      }
    ));
  }

   register(user): Observable<String>{
     const router = '/1/users/';
     return this.apiService.post('/userapi' + router, user)
     .pipe(map(
       data => {
         return data;
       }
     ))
   }

   adminAttemptAuth(credentials): Observable<User> {
    const route = '/auth/token';
    return this.apiService.post('/api' + route, credentials)
      .pipe(map(
      data => {
        this.setAuth(data);
        return data;
      }
    ));
  }

  forgotPassword(details): Observable<string> {

    return this.apiService.post(urlsUser.forgotPass,details)
      .pipe(map( 
        res => {
          return res;
        }
    ));
  }

  getAllUserTypes(data): Observable<string>{
    const router = '/1/usertypeapi/';
     return this.apiService.post('/userapi' + router, data)
     .pipe(map(
       data => {
         return data;
       }
     ))
  }

  

}
