import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService, UserService, JwtService } from './services';
import { AdminService } from './services/admin.user.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    ApiService,
    UserService,
    AdminService,
    JwtService
  ]
})
export class CoreModule { }
