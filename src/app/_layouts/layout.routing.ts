import { Routes, RouterModule } from '@angular/router';


export const ALL_ROUTES: Object = {

  admin : [
    {
      path: '',
      loadChildren: './pages/admin/admin.module#AdminModule'
    },
  ],
  coach : [
    {
      path: '',
      loadChildren: './pages/coach/coach.module#CoachModule'
    },
  ],

  trainer : [
    {
      path: '',
      loadChildren: './pages/trainer/trainer.module#TrainerModule'
    },
  ],

  seeker : [
    {
      path: '',
      loadChildren: './pages/seeker/seeker.module#SeekerModule'
    },
  ],
  mentor : [
    {
      path: '',
      loadChildren: './pages/mentor/mentor.module#MentorModule'
    },
  ]

};

