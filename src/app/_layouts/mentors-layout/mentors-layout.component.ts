import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../helpers';

@Component({
  selector: '.page-wrapper',
  templateUrl: './mentors-layout.component.html',
  styleUrls: ['./mentors-layout.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MentorsLayoutComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit() {
    Helpers.initLayout();
  }
}
