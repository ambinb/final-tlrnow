import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import {Helpers} from '../../helpers';

@Component({
  selector: '.page-wrapper',
  templateUrl: './coach-layout.component.html',
  styleUrls: ['./coach-layout.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CoachLayoutComponent {

  constructor() { }

  ngAfterViewInit() {

    // initialize layout: handlers, menu ...
    Helpers.initLayout();

  }

}
