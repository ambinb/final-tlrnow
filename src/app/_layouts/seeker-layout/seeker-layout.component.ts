import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../helpers';

@Component({
  selector: '.page-wrapper',
  templateUrl: './seeker-layout.component.html',
  styleUrls: ['./seeker-layout.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SeekerLayoutComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit() {
    Helpers.initLayout();
  }
}
