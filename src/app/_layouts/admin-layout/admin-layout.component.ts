import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import {Helpers} from '../../helpers';

@Component({
  selector: '.page-wrapper',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AdminLayoutComponent implements AfterViewInit{

  constructor() { }

  ngAfterViewInit() {

    // initialize layout: handlers, menu ...
    Helpers.initLayout();

  }
}
