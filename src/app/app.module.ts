import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './/app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LayoutModule } from './/layouts/layout.module';
import { ScriptLoaderService } from './_services/script-loader.service';
import { AdminModule } from './pages/admin/admin.module';
import { AdminLayoutComponent } from './_layouts/admin-layout/admin-layout.component';
import { AppHeader } from './layouts/app-header/app-header.component';
import { AppFooter } from './layouts/app-footer/app-footer.component';
import { AppSidebar } from './layouts/app-sidebar/app-sidebar.component';
import { CoachModule } from './pages/coach/coach.module';
import { CoachLayoutComponent } from './_layouts/coach-layout/coach-layout.component';
import { TrainerModule } from './pages/trainer/trainer.module';
import { TrainerLayoutComponent } from './_layouts/trainer-layout/trainer-layout.component';
import { SeekerModule } from './pages/seeker/seeker.module';
import { SeekerLayoutComponent } from './_layouts/seeker-layout/seeker-layout.component';
import { MentorModule } from './pages/mentor/mentor.module';
import { MentorsLayoutComponent } from './_layouts/mentors-layout/mentors-layout.component';
import { AddSessionsComponent } from './pages/add-sessions/add-sessions.component';
import { SharedModuleModule } from './shared-module/shared-module.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CoreModule } from './core';
import { SharedModule } from './pages/shared';
import { AuthGuard } from './core/services/auth-guard.service';
import { Auth } from './core/services/auth.service';
import { AdminAuthGuard } from './core/services/admin-auth-guard.service';
// import { ComplimentsComponent } from './pages/compliments/compliments.component';
// import { RequestNewroleComponent } from './pages/request-newrole/request-newrole.component';
// import { SwitchRoleComponent } from './pages/switch-role/switch-role.component';
// import { CourseDetailsComponent } from './pages/course-details/course-details.component';
// import { CourseDetailsComponent } from './pages/course-details/course-details.component';
// import { CourseComponent } from './pages/course/course.component';
// import { StartSessionsComponent } from './pages/start-sessions/start-sessions.component';
// import { ScheduleSessionsComponent } from './pages/schedule-sessions/schedule-sessions.component';
// import { AddCourseComponent } from './pages/add-course/add-course.component';
// import { Component } from './pages/.component';
// import { AddCourseComponent } from './pages/add-course/add-course.component';
// import { CourseSelectComponent } from './pages/course-select/course-select.component';



@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    CoachLayoutComponent,
    TrainerLayoutComponent,
    SeekerLayoutComponent,
    MentorsLayoutComponent,
    AddSessionsComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    CoreModule,
    SharedModuleModule,
    AppRoutingModule,
    LayoutModule,
    AdminModule,
    CoachModule,
    TrainerModule,
    SeekerModule,
    MentorModule
  ],
  providers: [ScriptLoaderService, Auth, AuthGuard, AdminAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
