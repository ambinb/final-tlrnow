import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';

@Component({
  selector: 'app-trainer-dashboard',
  templateUrl: './trainer-dashboard.component.html',
  styleUrls: ['./trainer-dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TrainerDashboardComponent implements AfterViewInit {

  constructor(private _script: ScriptLoaderService) { }

  ngAfterViewInit() {
    this._script.load('./assets/js/scripts/dashboard_visitors.js');
  }
}
