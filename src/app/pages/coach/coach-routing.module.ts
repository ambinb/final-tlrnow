import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoachDashboardComponent } from './coach-dashboard/coach-dashboard.component';
import { CourseComponent } from '../course/course.component';
// import { CourseDetailsComponent } from '../course-details/course-details.component';
import { ScheduleSessionsComponent } from '../schedule-sessions/schedule-sessions.component';
import { StartSessionsComponent } from '../start-sessions/start-sessions.component';
import { CourseDetailsComponent } from '../course-details/course-details.component';
import { AddCourseComponent } from '../add-course/add-course.component';
import { ComplimentsComponent } from '../compliments/compliments.component';


const routes: Routes = [
  {
    path: "",
    redirectTo:'dashboard',
    pathMatch:'full'
  },
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: CoachDashboardComponent
      },
      {
        path: 'course',
        component: CourseComponent
      },
      {
        path: 'course-details',
        component: CourseDetailsComponent
      },
      {
        path: 'add-course',
        component: AddCourseComponent
      },
      {
        path: 'schedule-sessions',
        component: ScheduleSessionsComponent
      },
      {
        path: 'start-sessions',
        component: StartSessionsComponent
      },
      {
        path: 'Compliments',
        component: ComplimentsComponent
      },
     
    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoachRoutingModule { }
