import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-user-type',
  templateUrl: './user-type.component.html',
  styleUrls: ['./user-type.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserTypeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
