import { Component, OnInit, AfterViewInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { appVariables } from '../../app-constants';

import { Errors, AdminService } from '../../core'

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  errors;
  isSubmitting = false;
  adminAuthForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private adminService: AdminService
  ) {

    this.adminAuthForm = this.fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required]
    });

  }

  get username() { return this.adminAuthForm.get('username'); }
  get password() { return this.adminAuthForm.get('password'); }


  submitForm() {
    this.isSubmitting = true;
    this.errors = '';

    const credentials = this.adminAuthForm.value;

    this.adminService.attemptAuth(credentials)
      .subscribe(res => {
        console.log('res: ', res);
        this.router.navigateByUrl('/admin/dashboard');
      },
        err => {
          this.errors = err;
          console.log('this.errors: ', this.errors);
        }
      )

  }

  ngOnInit() {
  }

}
