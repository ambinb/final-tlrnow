import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-compliments',
  templateUrl: './compliments.component.html',
  styleUrls: ['./compliments.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class ComplimentsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
