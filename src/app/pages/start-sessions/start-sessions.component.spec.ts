import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartSessionsComponent } from './start-sessions.component';

describe('StartSessionsComponent', () => {
  let component: StartSessionsComponent;
  let fixture: ComponentFixture<StartSessionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartSessionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartSessionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
