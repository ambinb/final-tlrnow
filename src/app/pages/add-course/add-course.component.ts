import { Component, OnInit, ViewEncapsulation, AfterViewInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrComponent } from '../ui/toastr/toastr.component';
import { appVariables } from '../../app-constants';
import { Errors, UserService } from '../../core'
import { Router } from '@angular/router';
import { CustomValidators } from 'ng4-validators';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddCourseComponent implements OnInit {

  selectedFile: File = null;
  errors = '';
  isSubmitting = false;
  createCourseForm: FormGroup;

  url: String = "./assets/img/course/course.jpg";

  constructor(private fb: FormBuilder,
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
  }

  onFileSelect(event) {

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.url = event.target.result;
      }

      this.selectedFile = event.target.files[0];

      reader.readAsDataURL(event.target.files[0]);
    }

   
  }

  submitForm(){

  }

}
