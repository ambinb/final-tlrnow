import { Component, OnInit, ViewEncapsulation, AfterViewInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrComponent } from '../ui/toastr/toastr.component';
import { appVariables } from '../../app-constants';
import { Errors, UserService } from '../../core'
import { Router } from '@angular/router';
import { CustomValidators } from 'ng4-validators';

declare var $: any;
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class RegisterComponent implements OnInit, AfterViewInit, OnDestroy {


  errors = '';
  isSubmitting = false;
  signUpForm: FormGroup;
  isNotResponse = true;
  result;
  isConfirmPasswordError = true;
  userTypesList;



  constructor(private fb: FormBuilder,
    private router: Router,
    private userService: UserService) {

    let userpass = new FormControl('', Validators.required);
    let certainPassword = new FormControl('', CustomValidators.notEqualTo(userpass));

    this.signUpForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      userType: ['', Validators.required],
      emailid: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      userpass: ['', [Validators.required, Validators.minLength(8)]],
      conPassword: ['', [Validators.required, Validators.minLength(8)]]
    });

  }

  get name() { return this.signUpForm.get('name'); }

  get userType() { return this.signUpForm.get('userType'); }

  get email() { return this.signUpForm.get('emailid'); }

  get password() { return this.signUpForm.get('userpass'); }

  get cnPassword() { return this.signUpForm.get('conPassword'); }

  ngOnInit() {
    this.userTypes();
    $('body').addClass('empty-layout');
  }

  ngAfterViewInit() {
    $('#cnpass').on('keyup', function () {
      let cpass = $('#cpass').val();
      let cnpass = $(this).val();
      if (cpass === cnpass) {
        $('#regBtn').removeAttr('disabled');
      } else {
        $('#regBtn').attr('disabled', 'disabled');
      }
    });
  }

  ngOnDestroy() {
    $('body').removeClass('empty-layout');
  }

  comparePassword() {
    const formData = this.signUpForm.value;
    //confirmPasswordError
    console.log('this.signUpForm.valid: ', this.signUpForm.valid);
    if (formData.userpass === formData.conPassword) {
      this.isConfirmPasswordError = true;
      return true;
    } else {
      this.isConfirmPasswordError = false;
      return false;
    }

  }

  userTypes(){
    this.isSubmitting = true;
    this.errors = '';
    const data = {
      "action":"alllist"
     }
    this.userService.getAllUserTypes(data)
      .subscribe(
        res => {
          console.log('res: ', res);
          if(res && res['responsecode'] == 200){
            this.userTypesList = res['userTypeList'];
          }
          this.isNotResponse = false;
        },
        err => {
          this.errors = err;
          this.isSubmitting = false;
        }
      )
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = '';

    const userInput = this.signUpForm.value;
    const user = {
      "action": "create",
      "userType": userInput.userType,
      "name": userInput.name,
      "emailid": userInput.emailid,
      "userpass": userInput.userpass,
      "primaryrole": "1"
    }
    console.log('user: ', user);
    this.userService.register(user)
      .subscribe(
        res => {
          console.log('res: ', res);
          this.isNotResponse = false;
          this.result = res;
        },
        err => {
          this.errors = err;
          this.isSubmitting = false;
        }
      )



  }

}

