import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';

@Component({
  selector: 'app-mentor-dashboard',
  templateUrl: './mentor-dashboard.component.html',
  styleUrls: ['./mentor-dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MentorDashboardComponent implements AfterViewInit {

  constructor(private _script: ScriptLoaderService) { }

  ngAfterViewInit() {
    this._script.load('./assets/js/scripts/dashboard_visitors.js');
  }
}
