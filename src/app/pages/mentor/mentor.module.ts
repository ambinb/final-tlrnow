import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MentorRoutingModule } from './mentor-routing.module';
import { MentorDashboardComponent } from './mentor-dashboard/mentor-dashboard.component';
import { MentorHeaderComponent } from './_layout/mentor-header/mentor-header.component';
import { MentorSidebarComponent } from './_layout/mentor-sidebar/mentor-sidebar.component';
import { SharedModuleModule } from '../../shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    MentorRoutingModule,SharedModuleModule
  ],
  exports:[
    MentorSidebarComponent, MentorHeaderComponent
  ],
  declarations: [MentorDashboardComponent, MentorHeaderComponent, MentorSidebarComponent]
})

export class MentorModule {

 }
