import { Component, OnInit, AfterViewInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { appVariables } from '../../app-constants';

import { Errors, AdminService } from '../../core'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  errors;
  isSubmitting = false;
  userList;
  successMsg;
  userTypesList;

  constructor(private router: Router,
    private adminService: AdminService) { }

  ngOnInit() {
    this.getUsers();
    this.userTypes();
  }

  deleteUser(id) {
    console.log('id: ', id);
    this.isSubmitting = true;
    this.errors = '';

    let data = {
      action: "delete",
      uId: id['uId']
    }

    this.adminService.manageUsers(data)
      .subscribe(res => {
        console.log('res: ', res);
        if (res && res['responsecode'] == 200) {
          this.getUsers();
          this.successMsg = res['status']

        }

      },
        err => {
          this.errors = err;
          console.log('this.errors: ', this.errors);
        }
      )
  }

  getUsers() {
    this.isSubmitting = true;
    this.errors = '';
    let data = { action: "alllist" }

    this.adminService.manageUsers(data)
      .subscribe(res => {
        console.log('res: ', res);
        if (res && res['responsecode'] == 200) {
          this.userList = res['userList'];
          console.log('this.userList : ', this.userList);
        }

      },
        err => {
          this.errors = err;
          console.log('this.errors: ', this.errors);
        }
      )

  }

  userTypes(){
    this.isSubmitting = true;
    this.errors = '';
    const data = {
      "action":"alllist"
     }
    this.adminService.getAllUserTypes(data)
      .subscribe(
        res => {
          console.log('res: ', res);
          if(res && res['responsecode'] == 200){
            this.userTypesList = res['userTypeList'];
          }
        },
        err => {
          this.errors = err;
          this.isSubmitting = false;
        }
      )
  }

}
